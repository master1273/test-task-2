//
//  VideosCollectionViewCell.swift
//  Test Task 2
//
//  Created by Admin on 9/26/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Photos
import AVKit
import AVFoundation

class VideosCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var thumbnailImageView: UIImageView!
    
    var avPlayerController: AVPlayerViewController?
    
    var video: PHAsset!{
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        avPlayerController = AVPlayerViewController()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(playOrPauseVideo))
        thumbnailImageView.isUserInteractionEnabled = true
        thumbnailImageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func updateUI(){
        let asset = video
        let targetSize = CGSize(width: 100, height: 100)
        let contentModel = PHImageContentMode.aspectFit
        requestImage(for: asset!, targetSize: targetSize, contentMode: contentModel, completionHandler: { image in
            self.thumbnailImageView.image = image
        })
    }
    
    func requestImage(for asset: PHAsset,
                      targetSize: CGSize,
                      contentMode: PHImageContentMode,
                      completionHandler: @escaping (UIImage?) -> ()) {
        let imageManager = PHImageManager()
        imageManager.requestImage(for: asset,
                                  targetSize: targetSize,
                                  contentMode: contentMode,
                                  options: nil) { (image, _) in
                                    completionHandler(image)
        }
    }
    
    @objc func playOrPauseVideo() {
        guard let player = avPlayerController!.player else {
            setupVideo(asset: video)
            return
        }
        
        if (player.rate == 0){
//            playVideo(asset: video)
            player.play()
        } else {
            player.pause()
        }
    }
    
    override func prepareForReuse() {
        self.avPlayerController!.player?.pause()
        self.avPlayerController!.player = nil
    }
    
    func setupVideo (asset: PHAsset) {
        guard (asset.mediaType == PHAssetMediaType.video) else {
            print("Not a valid video media type")
            return
        }
        
        PHCachingImageManager().requestAVAsset(forVideo: asset, options: nil, resultHandler: {
            (asset: AVAsset?, audioMix: AVAudioMix?, info: [AnyHashable : Any]?) in
            
            let asset = asset as! AVURLAsset
            
            DispatchQueue.main.async {
                let player = AVPlayer(url: asset.url)
                self.avPlayerController!.player = player
                self.avPlayerController!.showsPlaybackControls = false
                
                self.avPlayerController!.view.frame = self.thumbnailImageView.bounds
                self.addSubview(self.avPlayerController!.view)
                
                
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.playOrPauseVideo))
                self.avPlayerController!.view.isUserInteractionEnabled = true
                self.avPlayerController!.view.addGestureRecognizer(tapGestureRecognizer)
                
                self.playOrPauseVideo()
                //self.avPlayerController!.player?.play()
                //add playerController view as subview to cell
            }
        })
    }
}
