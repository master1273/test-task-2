//
//  AudiosCell.swift
//  Test Task 2
//
//  Created by Admin on 10/3/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class AudiosCell: UICollectionViewCell {
    
    @IBOutlet weak var thumbnailView: UIImageView!
    @IBOutlet weak var trackNameLabel: UILabel!
    @IBOutlet weak var albumNameLabel: UILabel!
    
    var musicTrack: MusicTrack!{
        didSet {
            self.updateUI()
        }
    }
    
    func updateUI(){
        trackNameLabel.text = musicTrack.title
        albumNameLabel.text = musicTrack.album
        
        thumbnailView.image = nil
        if let url = musicTrack.thumbnail{
            let request = URLRequest(url: url)
            let networkProcessor = NetworkProcessor(request: request)
            
            networkProcessor.downloadData{
                (data, response, error) in
                
                DispatchQueue.main.async {
                    if let imageData = data {
                        self.thumbnailView.image = UIImage(data: imageData)
                    }
                }
                
            }
        }
    }
}
