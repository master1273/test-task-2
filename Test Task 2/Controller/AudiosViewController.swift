//
//  AudiosViewController.swift
//  Test Task 2
//
//  Created by Admin on 10/3/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import AVFoundation

class AudiosViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var progress: UIView!
    
    @IBOutlet weak var progressView: UISlider!
    
    let gridColumnLayout = ColumnFlowLayout(
        cellsPerRow: 2,
        minimumInteritemSpacing: 10,
        minimumLineSpacing: 10,
        sectionInset: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    )
    
    var tracks : [MusicTrack]?
    var iTunesClient = ITunesClient()
    
    var playerLayer: AVPlayerLayer?
    var playerItem: AVPlayerItem?
    var player: AVPlayer?
    
    struct Storyboard {
        static let trackCell = "TrackCell"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(finishedPlaying), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.collectionViewLayout = gridColumnLayout
        
        progress.isHidden = true
        
        fetchApps()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    func fetchApps(){
        iTunesClient.fetchMusic(withTerm: "holiday", inEntity: "musicTrack"){
            (tracks) in
            
            self.tracks = tracks
            self.collectionView.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let tracks = tracks {
            return tracks.count
        }
        
        return 0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Storyboard.trackCell, for: indexPath) as! AudiosCell
        
        cell.musicTrack = tracks?[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        play(url: tracks![indexPath.row].trackURL)
    }
    
    private func play(url: URL?){
        guard let url = url else{
            return
        }
        
        if (player != nil && player!.rate != 0){
            player!.pause()
        }
        
        playerItem = AVPlayerItem(url: url)
        player = AVPlayer(playerItem: playerItem)
        
        playerLayer?.removeFromSuperlayer()
        
        playerLayer = AVPlayerLayer(player: player)
        playerLayer!.frame = CGRect(x: 0, y: 0, width: 10, height: 50)
        self.view.layer.addSublayer(playerLayer!)
        
        // Add playback slider
        
        progressView.minimumValue = 0
        
        
        let duration : CMTime = playerItem!.asset.duration
        let seconds : Float64 = CMTimeGetSeconds(duration)
        
        progressView.maximumValue = Float(seconds)
        progressView.isContinuous = false
        
//        progressView.addTarget(self, action: #selector(playbackSliderValueChanged), for: .valueChanged)
        
        player!.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: DispatchQueue.main) { (CMTime) -> Void in
            if self.player!.currentItem?.status == .readyToPlay {
                let time : Float64 = CMTimeGetSeconds(self.player!.currentTime());
                self.progressView.value = Float ( time );
            }
        }
        
        player!.play()
    
        progress.isHidden = false
        
    }
    
//    @objc func playbackSliderValueChanged(_ playbackSlider:UISlider){
//
//        let seconds : Int64 = Int64(playbackSlider.value)
//        let targetTime:CMTime = CMTimeMake(value: seconds, timescale: 1)
//
//        player!.seek(to: targetTime)
//
//        if player!.rate == 0
//        {
//            player?.play()
//        }
//    }
//
    @objc func finishedPlaying(myNotification:NSNotification){
           progress.isHidden = true
    }
    
    @IBAction func stopPlayback(_ sender: Any) {
        player?.pause()
        progress.isHidden = true
    }
}
