//
//  VideosCollectionViewController.swift
//  Test Task 2
//
//  Created by Admin on 9/26/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Photos

private let reuseIdentifier = "Cell"

class VideosCollectionViewController: UICollectionViewController {
    
    private var isList = false
    
    let gridColumnLayout = ColumnFlowLayout(
        cellsPerRow: 2,
        minimumInteritemSpacing: 10,
        minimumLineSpacing: 10,
        sectionInset: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    )
    
    let listColumnLayout = ColumnFlowLayout(
        cellsPerRow: 1,
        minimumInteritemSpacing: 10,
        minimumLineSpacing: 10,
        sectionInset: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    )
    
    struct Storyboard {
        static let videoCell = "VideoCell"
    }
    
    var fetchResults: PHFetchResult<PHAsset>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        setupCollectionView()
        // Register cell classes
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        fetchResults = PHAsset.fetchAssets(with: .video, options: nil)
        
        if (fetchResults.count > 0){
            collectionView.reloadData()
        }
        
        let listSwitchButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(listSwitch))
        self.navigationItem.rightBarButtonItem = listSwitchButton
    }
    
    @objc private func listSwitch(){
        isList = !isList
        setupCollectionView()
        collectionView.reloadData()
    }
    
    private func setupCollectionView(){
        if (isList){
            collectionView!.collectionViewLayout = listColumnLayout
        } else {
            collectionView!.collectionViewLayout = gridColumnLayout
        }
        
        if #available(iOS 11.0, *) {
            collectionView!.contentInsetAdjustmentBehavior = .always
        }
    }
    
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let fetchResults = fetchResults {
            return fetchResults.count
        }
        
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Storyboard.videoCell, for: indexPath) as! VideosCollectionViewCell
        
        cell.video = fetchResults?[indexPath.row]
        
        return cell
    }
}
