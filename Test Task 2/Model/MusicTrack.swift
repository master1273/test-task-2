//
//  MusicTrack.swift
//  Test Task 2
//
//  Created by Admin on 9/20/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

struct MusicTrack {
    var title: String
    var album: String
    var thumbnail: URL?
    var trackURL: URL?
    
    private struct APIKeys {
        static let trackName = "trackName"
        static let collectionName = "collectionName"
        static let artworkUrl = "artworkUrl100"
        static let previewUrl = "previewUrl"
    }
    
    init?(dictionary : [String : Any]){
        guard
            let title = dictionary[APIKeys.trackName] as? String,
            let album = dictionary[APIKeys.collectionName] as? String,
            let thumbnail = dictionary[APIKeys.artworkUrl] as? String,
            let trackURL = dictionary[APIKeys.previewUrl] as? String else {
                return nil
        }
        
        self.title = title
        self.album = album
        self.thumbnail = URL(string: thumbnail)
        self.trackURL = URL(string: trackURL)
    }
}
