//
//  ITunesClient.swift
//  Test Task 2
//
//  Created by Admin on 9/20/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

struct ITunesClient{
    
    func fetchMusic(withTerm term: String, inEntity entity: String, completion: @escaping ([MusicTrack]?) -> Void){
        
        let searchEndpoint = ITunesEndpoint.search(term: term, entity: entity)
        let searchUrlRequest = searchEndpoint.request
        
        let networkProcessor = NetworkProcessor(request: searchUrlRequest)
        networkProcessor.downloadJSON{(jsonResponse, httpResponse, error) in
            
            
            DispatchQueue.main.async {
                guard let json = jsonResponse, let resultDictionaries = json["results"] as? [[String : Any]] else {
                    completion(nil)
                    return
                }
                
                var tracks = [MusicTrack]()
                for trackDictionary in resultDictionaries {
                    let track = MusicTrack(dictionary: trackDictionary)!
                    tracks.append(track)
                }
                
                completion(tracks)
            }
        }
        
    }
    
}
