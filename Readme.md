##iOS Programming Test Task 2

###Requirements.

Target iOS version is 8.0.
Target iOS device is iPhone. Should work correctly on all iPhone screen types starting from 3.5’’.
Use either Objective C or Swift language for implementation, the one you feel yourself better in.
Use assets catalogue to keep all resources used.
Use storyboards for UI implementation. Do not do any manual layouts in the code.
You are not allowed to use 3rdParty libraries and code, only components from IOS SDK.


###Workflow.
    1.	Launch Screen. When application launches launch screen should be presented to the user.
        a. Launch screen should have sky blue background
        b. Launch screen should have large centered black text “Test Task 2”
        c. Launch screen should have small black text “All rights reserved” centered on the bottom
    2.	Start Screen. Should be the first screen that user sees on launch.
        a. There should be 2 buttons on Start Screen: Audios and Videos
        b. When Audios button is pressed user should be taken to Audios list screen.
        c. When Videos button is pressed user should be taken to Videos list screen.
    3.	Audios List Screen. Should fetch and display in grid information about audio items searched by term “Holiday” in iTunes. Use apple documentation for details: https://www.apple.com/itunes/affiliates/resources/documentation/itunes-store-web-service-search-api.html
        a. Display items in two columns
        b. For each item display thumbnail image, album and title labels under it
        c. When tapping any item start playing it, display playing progress somehow either on the item or on the screen itself
        d. There should be a way to stop playing
    4.	Videos List Screen. Should load all videos from camera roll and display them.
        a. There should be two view modes: grid and list. In grid use 2 columns for items in list - only 1 column
        b. Items should only contain video thumbnail
        c. When thumbnail is tapped appropriate video should start playing instead of still thumbnail
        d. To switch between grid and list modes there should be a button on the right on the navigation bar